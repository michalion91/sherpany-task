# Michal Szulc Address Book app

Hello and welcome to my repo. Please find below list, describing available commands.

I'm looking forward to discuss this with you!

## Prerequisites

- node 10+ (ideally 12+)
- preferably yarn (or npm)
- `yarn` or `npm install` executed in repo root dir

## Available commands

In doubt please consult `package.json` runscripts. Code never lies.

### start

Run application in development mode. Will automatically open browser page.

### build

Will create production grade build in `dist/` folder.

### test

Runs jest test suite once. Used in on-commit checks.

### test:watch

Runs jest test suite in watch mode

### test:mutation

Runs `stryker` mutation testing library. Will generate a html report. Takes a long time (in comparison to other commands)

### test:typescript

Runs `tsc` without emitting to ensure validity of the application typings outside webpack. Used in on-commit checks.

### eslint

Checks code validity against eslint-rules. Warnings will result in error. Used in on-commit checks.

### prettier

Checks code formatting. Will error out on violations. Used in on-commit checks.
