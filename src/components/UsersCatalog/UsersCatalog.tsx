import React from "react";
import Card from "react-bootstrap/Card";

import UserItem from "../UserItem";
import IntersectionAware from "../IntersectionAware";
import { User } from "network";

import "./UsersCatalog.css";

interface UserCatalogProps {
  users: User[];
  filterOn: boolean;
  loadMoreData: () => void;
  catalogEnd: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
const noOp = () => {};

const UserCatalog: React.FC<UserCatalogProps> = (props: UserCatalogProps) => {
  return (
    <div className="users-catalog-container">
      {props.users.map((user, index, array) => {
        if (!props.catalogEnd && index + 1 === array.length) {
          return (
            <IntersectionAware
              key={user.login.uuid}
              onVisible={props.filterOn ? noOp : props.loadMoreData}
            >
              <UserItem user={user} />
            </IntersectionAware>
          );
        }
        return <UserItem key={user.login.uuid} user={user} />;
      })}
      {props.catalogEnd && (
        <>
          <div className="user-catalog-end-spacer"></div>
          <Card bg="danger" text="white">
            <Card.Body>End of catalogue</Card.Body>
          </Card>
        </>
      )}
    </div>
  );
};

export default UserCatalog;
