import { connect } from "react-redux";

import { loadNextUsers } from "~/state/thunks";
import { ApplicationState } from "~/state/reducer";
import {
  selectVisibleUsers,
  isEndOfCatalogue,
  selectSearchQuery,
} from "~/state/selectors";

import UsersCatalog from "./UsersCatalog";

// NOTE: blatantly copied from https://redux.js.org/recipes/usage-with-typescript#typing-the-connect-higher-order-component
const mapState = (state: ApplicationState) => {
  const nonFilteredVisibleUsers = selectVisibleUsers(state);
  const filterQuery = selectSearchQuery(state);

  return {
    filterOn: Boolean(filterQuery),
    users: filterQuery
      ? nonFilteredVisibleUsers.filter(({ name: { first, last } }) =>
          `${first.toLowerCase()} ${last.toLowerCase()}`.includes(
            filterQuery.toLowerCase()
          )
        )
      : nonFilteredVisibleUsers,
    catalogEnd: isEndOfCatalogue(state),
  };
};

const mapDispatch = {
  loadMoreData: loadNextUsers,
};

const connector = connect(mapState, mapDispatch);

export default connector(UsersCatalog);
