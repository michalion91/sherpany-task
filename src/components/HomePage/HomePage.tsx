import React from "react";

import UserCatalog from "~/components/UsersCatalog";

const HomePage: React.FC = () => {
  return <UserCatalog />;
};

export default HomePage;
