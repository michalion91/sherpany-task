import React, { useEffect, ReactNode } from "react";

interface IntersectionAwareProps {
  children: ReactNode;
  onVisible: () => void;
}

const IntersectionAware = (props: IntersectionAwareProps): JSX.Element => {
  const ref = React.createRef<HTMLDivElement>();
  const observer = new IntersectionObserver(([element]) => {
    if (element.isIntersecting) {
      props.onVisible();
      observer.disconnect();
    }
  });
  useEffect(() => {
    if (ref.current) {
      observer.observe(ref.current);
    }
  }, [ref.current]);

  return <div ref={ref}>{props.children}</div>;
};

export default IntersectionAware;
