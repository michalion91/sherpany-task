import React from "react";
import { Route, Switch } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";

import store from "~/state/store";
import { appStartup } from "~/state/thunks";
import { ApplicationState } from "~/state/reducer";
import { LoadInitialDataAction } from "~/state/types";

import HomePage from "./HomePage";
import SettingsPage from "./SettingsPage";
import Topbar from "~/components/Topbar";

const App: React.FC = () => {
  (store.dispatch as ThunkDispatch<
    ApplicationState,
    void,
    LoadInitialDataAction
  >)(appStartup());

  return (
    <>
      <Topbar />
      <Switch>
        <Route path="/settings" component={SettingsPage} />
        <Route exact path="/" component={HomePage} />
      </Switch>
    </>
  );
};

export default App;
