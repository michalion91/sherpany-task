import React from "react";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";

import { SupportedNationality } from "~/state/types";

export interface SettingsPageProps {
  currentNationality: SupportedNationality;
  changeNationality: (arg: SupportedNationality) => void;
}
const SettingsPage: React.FC<SettingsPageProps> = (
  props: SettingsPageProps
) => {
  return (
    <>
      <p>Choose nationality</p>
      <ButtonGroup>
        <Button
          variant={computeVariant(
            SupportedNationality.CH,
            props.currentNationality
          )}
          onClick={() => props.changeNationality(SupportedNationality.CH)}
        >
          Swiss
        </Button>
        <Button
          variant={computeVariant(
            SupportedNationality.FR,
            props.currentNationality
          )}
          onClick={() => props.changeNationality(SupportedNationality.FR)}
        >
          French
        </Button>
        <Button
          variant={computeVariant(
            SupportedNationality.ES,
            props.currentNationality
          )}
          onClick={() => props.changeNationality(SupportedNationality.ES)}
        >
          Spanish
        </Button>
        <Button
          variant={computeVariant(
            SupportedNationality.GB,
            props.currentNationality
          )}
          onClick={() => props.changeNationality(SupportedNationality.GB)}
        >
          British
        </Button>
      </ButtonGroup>
    </>
  );
};

function computeVariant(
  expected: SupportedNationality,
  actual: SupportedNationality
): string {
  return actual === expected ? "primary" : "secondary";
}

export default SettingsPage;
