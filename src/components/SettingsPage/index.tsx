import { connect } from "react-redux";

import { ApplicationState } from "~/state/reducer";
import { selectNationality } from "~/state/selectors";
import { SupportedNationality } from "~/state/types";
import { changeNationality } from "~/state/thunks";

import SettingsPage from "./SettingsPage";

// NOTE: blatantly copied from https://redux.js.org/recipes/usage-with-typescript#typing-the-connect-higher-order-component
const mapState = (state: ApplicationState) => ({
  currentNationality: selectNationality(state),
});

const mapDispatch = {
  changeNationality: (newNationality: SupportedNationality) =>
    changeNationality(newNationality),
};

const connector = connect(mapState, mapDispatch);

export default connector(SettingsPage);
