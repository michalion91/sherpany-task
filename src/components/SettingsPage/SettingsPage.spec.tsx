import "jest";
import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { SupportedNationality } from "~/state/types";
import SettingsPage from "./SettingsPage";

describe("<SettingsPage />", () => {
  test("presents available nationalities", () => {
    render(
      <SettingsPage
        currentNationality={SupportedNationality.CH}
        changeNationality={() => ({})}
      />
    );

    expect(screen.getByText("Swiss")).toBeVisible();
    expect(screen.getByText("French")).toBeVisible();
    expect(screen.getByText("Spanish")).toBeVisible();
    expect(screen.getByText("British")).toBeVisible();
  });

  const testData: [string, SupportedNationality][] = [
    ["Swiss", SupportedNationality.CH],
    ["French", SupportedNationality.FR],
    ["Spanish", SupportedNationality.ES],
    ["British", SupportedNationality.GB],
  ];

  test.each(testData)(
    "triggers callback with %s nationality, accents active nationality",
    (currentLabel: string, currentNationality: SupportedNationality) => {
      expect.assertions(9);

      render(
        <SettingsPage
          currentNationality={currentNationality}
          changeNationality={(arg: SupportedNationality) => {
            expect(arg).toEqual(currentNationality);
          }}
        />
      );

      const { classList } = screen.getByText(currentLabel);
      expect(classList.contains("btn-primary")).toBeTruthy();
      expect(classList.contains("btn-secondary")).not.toBeTruthy();

      testData
        .map(([labelText]) => labelText)
        .filter((labelText) => labelText !== currentLabel)
        .forEach((labelText) => {
          const { classList } = screen.getByText(labelText);
          expect(classList.contains("btn-primary")).not.toBeTruthy();
          expect(classList.contains("btn-secondary")).toBeTruthy();
        });

      fireEvent.click(screen.getByText(currentLabel));
    }
  );
});
