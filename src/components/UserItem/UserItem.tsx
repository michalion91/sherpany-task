import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import Modal from "react-bootstrap/Modal";
import ListGroup from "react-bootstrap/ListGroup";
import Image from "react-bootstrap/Image";

import { User } from "~/network";

import "./UserItem.css";

interface UserItemProps {
  user: User;
}
const UserItem: React.FC<UserItemProps> = (props: UserItemProps) => {
  const {
    name: { first, last },
    picture: { thumbnail },
    email,
    login: { username },
    location: { street, city, postcode, state },
    cell,
    phone,
  } = props.user;

  const [show, setShow] = useState(false);

  return (
    <>
      <Card className="user-item-container">
        <Card.Header className="user-item-header">
          <Image roundedCircle src={thumbnail} />
          <h4>
            {first} {last}
          </h4>
        </Card.Header>
        <Card.Body>
          <div>username: {username}</div>
          <div>email: {email}</div>
        </Card.Body>
        <Card.Footer>
          <Card.Link onClick={() => setShow(true)}>Show details</Card.Link>
        </Card.Footer>
      </Card>
      <Modal
        animation={false}
        show={show}
        centered
        onHide={() => setShow(false)}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            {first} {last}
          </Modal.Title>
        </Modal.Header>
        <ListGroup variant="flush">
          <ListGroup.Item>
            <h5>Location</h5>
            <p>{`${street.name} ${street.number}`}</p>
            <p>{`${city} ${postcode}`}</p>
            <p>{state}</p>
          </ListGroup.Item>
          <ListGroup.Item>
            <h5>Contact details</h5>
            <p>Cell: {cell} </p>
            <p>Phone: {phone} </p>
          </ListGroup.Item>
        </ListGroup>
      </Modal>
    </>
  );
};

export default UserItem;
