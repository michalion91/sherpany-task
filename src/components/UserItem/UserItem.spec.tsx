import "jest";
import React from "react";
import { render, fireEvent, screen, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import UserItem from "./UserItem";
import testUser from "~/state/__test__/mockUserGenerator";

describe("<UserItem />", () => {
  test("presents Card with basic information", () => {
    const user = testUser("1");
    render(<UserItem user={user} />);

    expect(getByPartialText(user.login.username)).toBeVisible();
    expect(getByPartialText(user.email)).toBeVisible();
    expect(
      screen.getByText(`${user.name.first} ${user.name.last}`)
    ).toBeVisible();

    expect(screen.getByRole("img")).toBeVisible();
  });

  test("allows user to open a modal with location and phone contact details and to close it", async () => {
    const user = testUser("1");
    render(<UserItem user={user} />);

    fireEvent.click(screen.getByText("Show details"));

    await waitFor(() =>
      expect(screen.getByText(user.location.state)).toBeVisible()
    );

    [
      getByPartialText(user.location.street.name),
      getByPartialText(`${user.location.street.number}`),
      getByPartialText(user.location.city),
      getByPartialText(`${user.location.postcode}`),
      getByPartialText(`${user.phone}`),
      getByPartialText(`${user.cell}`),
    ].forEach((element) => {
      expect(element).toBeVisible();
    });

    fireEvent.click(screen.getByRole("button"));

    expect(screen.queryByText(user.location.state)).not.toBeInTheDocument();
  });
});

function getByPartialText(text: string): HTMLElement {
  return screen.getByText(text, { exact: false });
}
