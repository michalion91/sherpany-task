import React from "react";
import { NavLink } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import Spinner from "react-bootstrap/Spinner";

import "./Topbar.css";

interface TopbarProps {
  isLoading: boolean;
  searchQuery: string;
  update: (fullName: string) => void;
}
const Topbar: React.FC<TopbarProps> = (props: TopbarProps) => {
  return (
    <Navbar sticky="top" bg="dark" variant="dark" expand="lg">
      <Navbar.Brand href="#home">Michal Szulc Address Book</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <NavLink
            className="topbar-nav-links"
            exact
            activeStyle={{ textDecoration: "underline" }}
            to="/"
          >
            Catalogue
          </NavLink>
          <NavLink className="topbar-nav-links" to="/settings">
            Settings
          </NavLink>
        </Nav>
      </Navbar.Collapse>
      {props.isLoading && (
        <div data-testid="spinner">
          <Spinner
            className="topbar-network-activity"
            animation="border"
            variant="light"
          />
        </div>
      )}

      {Boolean(props.searchQuery) && (
        <span className="topbar-network-suspended">Loading paused</span>
      )}
      <Form inline>
        {/* <FormControl type="text" placeholder="Search" className="mr-sm-2" /> */}
        <input
          placeholder="Search"
          onChange={({ target: { value } }) => props.update(value)}
          value={props.searchQuery}
        ></input>
      </Form>
    </Navbar>
  );
};

export default Topbar;
