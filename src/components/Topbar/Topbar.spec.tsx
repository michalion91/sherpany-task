import "jest";
import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";

import Topbar from "./Topbar";

const history = createMemoryHistory();
history.push = jest.fn();

const NavWrapper = (props: { children: JSX.Element }) => {
  return <Router history={history}>{props.children}</Router>;
};

describe("<Topbar />", () => {
  test("allows user to modify query (using callback) and informs about suspended network", () => {
    expect.assertions(4);

    const { rerender } = render(
      <NavWrapper>
        <Topbar isLoading={false} searchQuery="" update={() => ({})} />
      </NavWrapper>
    );

    expect(screen.getByText("Michal Szulc Address Book")).toBeVisible();

    const testText = "test";
    rerender(
      <NavWrapper>
        <Topbar
          isLoading={false}
          searchQuery={testText}
          update={(arg: string) => {
            expect(arg).toEqual("");
          }}
        />
      </NavWrapper>
    );

    const input = screen.getByDisplayValue(testText);
    expect(input).toBeVisible();

    fireEvent.change(input, { target: { value: "" } });

    expect(screen.getByText("Loading paused")).toBeVisible();
  });

  test("informs user about network activity", () => {
    render(
      <NavWrapper>
        <Topbar isLoading={true} searchQuery="" update={() => ({})} />
      </NavWrapper>
    );

    expect(screen.getByTestId("spinner")).toBeVisible();
  });
});
