import { connect } from "react-redux";

import { selectSearchQuery, isLoading } from "~/state/selectors";
import { ApplicationState } from "~/state/reducer";
import { updateQuery } from "~/state/actions";

import Topbar from "./Topbar";

const mapState = (state: ApplicationState) => ({
  searchQuery: selectSearchQuery(state),
  isLoading: isLoading(state),
});

const mapDispatch = {
  update: updateQuery,
};

const connector = connect(mapState, mapDispatch);

export default connector(Topbar);
