import "jest";
import React from "react";
import { MemoryRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { screen, render, waitFor, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import store from "~/state/store";
import App from "~/components/App";
import testUser from "~/state/__test__/mockUserGenerator";
import { User } from "~/network";
import { resetState } from "~/state/actions";

type FetchSignature = (
  arg1: RequestInfo,
  arg2?: RequestInit | undefined
) => Promise<Response>;

global.fetch = jest.fn() as FetchSignature;
global.IntersectionObserver = jest.fn(() => ({
  observe: jest.fn(),
  unobserve: jest.fn(),
})) as any; // eslint-disable-line @typescript-eslint/no-explicit-any

const user = testUser("1");

afterEach(() => {
  jest.restoreAllMocks();
  store.dispatch(resetState());
});

test("User enters the app and sees the catalogue", async (done) => {
  const users = new Array(100)
    .fill(0)
    .map((_val, index) => testUser(`${index}`));
  initialCallWillReturn(users);

  render(
    <Provider store={store}>
      <MemoryRouter>
        <App />
      </MemoryRouter>
    </Provider>
  );

  await waitFor(() =>
    expect(
      screen.queryAllByText(`${user.name.first} ${user.name.last}`).length
    ).toEqual(50)
  );
  done();
});

test("User enters the app and filters the catalogue", async (done) => {
  const users = new Array(100).fill(0).map((_val, index) => {
    if (index === 48) {
      return {
        ...testUser(`${index}`),
        name: {
          first: "search",
          last: "query",
        },
      };
    } else {
      return testUser(`${index}`);
    }
  });

  initialCallWillReturn(users);

  render(
    <Provider store={store}>
      <MemoryRouter>
        <App />
      </MemoryRouter>
    </Provider>
  );

  await waitFor(() =>
    expect(
      screen.queryAllByText(`${user.name.first} ${user.name.last}`).length
    ).toEqual(49)
  );

  expect(screen.getByText("search query")).toBeVisible();

  fireEvent.change(screen.getByPlaceholderText("Search"), {
    target: { value: "search query" },
  });

  await waitFor(() =>
    expect(
      screen.queryAllByText(`${user.name.first} ${user.name.last}`).length
    ).toEqual(0)
  );

  expect(screen.getByText("search query")).toBeVisible();
  done();
});

function initialCallWillReturn(arg: User[]) {
  (fetch as jest.Mock<unknown>).mockImplementation((url) => {
    expect(url).toEqual(
      "https://randomuser.me/api/?results=100&nationality=CH&page=1&inc=login%2Cname%2Clocation%2Cphone%2Ccell%2Cpicture%2Cemail"
    );

    return Promise.resolve({
      json: () => Promise.resolve({ results: arg }),
    });
  });
}
