import "jest";
import React from "react";
import { Provider } from "react-redux";
import { screen, render, waitFor, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import "@testing-library/jest-dom/extend-expect";

import store from "~/state/store";
import App from "~/components/App";
import testUser from "~/state/__test__/mockUserGenerator";
import { User } from "~/network";
import { SupportedNationality } from "~/state/types";

type FetchSignature = (
  arg1: RequestInfo,
  arg2?: RequestInit | undefined
) => Promise<Response>;

global.fetch = jest.fn() as FetchSignature;
global.IntersectionObserver = jest.fn(() => ({
  observe: jest.fn(),
  unobserve: jest.fn(),
})) as any; // eslint-disable-line @typescript-eslint/no-explicit-any

const user = testUser("1");

// NOTE: discussion point on refactoring
test("User enters the app, navigates to settings", async (done) => {
  const users = new Array(100)
    .fill(0)
    .map((_val, index) => testUser(`${index}`));

  networkCallWillReturn(users);

  render(
    <Provider store={store}>
      <MemoryRouter>
        <App />
      </MemoryRouter>
    </Provider>
  );

  await waitFor(() => cataloguePageVisible());

  goToSettings();

  await settingsPageVisible();

  networkCallWillReturn(users, SupportedNationality.GB);

  selectBritishNationality();
  goToCatalogue();

  await waitFor(cataloguePageVisible());

  done();
});

function cataloguePageVisible(): () => void {
  return () =>
    expect(
      screen.queryAllByText(`${user.name.first} ${user.name.last}`).length
    ).toEqual(50);
}

async function settingsPageVisible() {
  await waitFor(() => {
    expect(screen.getByText("Swiss")).toBeInTheDocument();
  });
}

function goToSettings() {
  fireEvent.click(screen.getByText("Settings"));
}

function selectBritishNationality() {
  fireEvent.click(screen.getByText("British"));
}

function goToCatalogue() {
  fireEvent.click(screen.getByText("Catalogue"));
}

function networkCallWillReturn(arg: User[], nationality = "CH") {
  (fetch as jest.Mock<unknown>).mockImplementationOnce((url) => {
    expect(url).toEqual(
      `https://randomuser.me/api/?results=100&nationality=${nationality}&page=1&inc=login%2Cname%2Clocation%2Cphone%2Ccell%2Cpicture%2Cemail`
    );

    return Promise.resolve({
      json: () => Promise.resolve({ results: arg }),
    });
  });
}
