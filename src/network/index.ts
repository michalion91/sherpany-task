const API_ORIGIN = "https://randomuser.me/api/";

export interface User {
  name: {
    first: string;
    last: string;
  };
  location: {
    street: {
      name: string;
      number: number;
    };
    city: string;
    state: string;
    postcode: number;
  };
  login: {
    uuid: string;
    username: string;
  };
  picture: {
    thumbnail: string;
  };
  cell: number;
  email: string;
  phone: number;
}

const constructURL = (nationality: string, page: number, size: number) => {
  const url = new URL(API_ORIGIN);

  url.searchParams.append("results", `${size}`);
  url.searchParams.append("nationality", nationality);
  url.searchParams.append("page", `${page}`);
  url.searchParams.append(
    "inc",
    "login,name,location,phone,cell,picture,email"
  );

  return url.href;
};

interface SuccessfulUserResponse {
  success: true;
  payload: User[];
}

interface ErrorUserResponse {
  success: false;
  payload: string;
}

const fetchUserBatch = (
  nationality: string,
  batch: number,
  size = 50
): Promise<SuccessfulUserResponse | ErrorUserResponse> =>
  fetch(constructURL(nationality, batch, size))
    .then((response) => response.json())
    .catch(() => {
      return {
        error: "Too many calls, please slow down",
      };
    })
    .then((data) =>
      data.error
        ? {
            success: false,
            payload: data.error,
          }
        : {
            success: true,
            payload: data.results,
          }
    );

export { fetchUserBatch };
