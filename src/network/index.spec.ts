import "jest";

import { fetchUserBatch, User } from "./index";

type FetchSignature = (
  arg1: RequestInfo,
  arg2?: RequestInit | undefined
) => Promise<Response>;

global.fetch = <FetchSignature>jest.fn();

const nationality = "ch";
const batch = 1;
const errorText = "Uh oh, something has gone wrong";

function validateUrlAndReturnPayload(
  expectedValue: { results: Partial<User>[] } | { error: string }
) {
  return (fetchConfig: RequestInfo) => {
    expect(fetchConfig).toEqual(
      `https://randomuser.me/api/?results=50&nationality=${nationality}&page=${batch}&inc=login%2Cname%2Clocation%2Cphone%2Ccell%2Cpicture%2Cemail`
    );

    return Promise.resolve({
      json: () => Promise.resolve(expectedValue),
    });
  };
}

describe("Network service", () => {
  test("provide client with relevant user details", async () => {
    const expectedValue = [
      {
        name: {
          first: "test",
          last: "text",
        },
      },
    ];

    (fetch as jest.Mock<unknown>).mockImplementationOnce(
      validateUrlAndReturnPayload({ results: expectedValue })
    );

    const result = await fetchUserBatch(nationality, batch);

    expect(result).toEqual({
      success: true,
      payload: expectedValue,
    });
  });

  test("informs client about failed response from API", async () => {
    (fetch as jest.Mock<unknown>).mockImplementationOnce(
      validateUrlAndReturnPayload({ error: errorText })
    );

    const result = await fetchUserBatch(nationality, batch);

    expect(result).toEqual({
      success: false,
      payload: errorText,
    });
  });

  test("informs client about network issues", async () => {
    (fetch as jest.Mock<unknown>).mockImplementationOnce(() => {
      return Promise.reject();
    });

    const result = await fetchUserBatch(nationality, batch);

    expect(result).toEqual({
      success: false,
      payload: "Too many calls, please slow down",
    });
  });
});

jest.restoreAllMocks();
