import { User } from "network";
import { SupportedNationality } from "./types";
import { ApplicationState } from "./reducer";

export const selectNationality = (
  state: ApplicationState
): SupportedNationality => state.nationality;

export const selectVisibleUsers = (state: ApplicationState): User[] =>
  state.visibleUsers;

export const selectBatchDetails = (
  state: ApplicationState
): { current: number; max: number } => ({
  current: state.currentBatch,
  max: state.maxBatch,
});

export const isEndOfCatalogue = (state: ApplicationState): boolean =>
  state.catalogueEnd;

export const selectSearchQuery = (state: ApplicationState): string =>
  state.searchQuery;

export const isLoading = (state: ApplicationState): boolean =>
  state.searchPending;
