import {
  ADD_NEXT_USERS,
  ApplicationActionTypes,
  END_OF_CATALOGUE,
  LOAD_INITIAL_DATA,
  SCHEDULE_SEARCH,
  SHOW_NEXT_USERS,
  SupportedNationality,
  UPDATE_NATIONALITY,
  UPDATE_QUERY,
  RESET_STATE,
} from "./types";
import { User } from "~/network";

export type ApplicationState = {
  allUsers: User[];
  catalogueEnd: boolean;
  currentBatch: number;
  maxBatch: number;
  nationality: SupportedNationality;
  searchPending: boolean;
  searchQuery: string;
  visibleUsers: User[];
};

export const INITIAL_STATE: ApplicationState = {
  allUsers: [],
  catalogueEnd: false,
  currentBatch: 1,
  maxBatch: 22,
  nationality: SupportedNationality.CH,
  searchPending: true,
  searchQuery: "",
  visibleUsers: [],
};

const applicationReducer = (
  state = INITIAL_STATE,
  action: ApplicationActionTypes
): ApplicationState => {
  switch (action.type) {
    case LOAD_INITIAL_DATA:
      return {
        ...state,
        currentBatch: 2,
        allUsers: action.payload,
        visibleUsers: action.payload.slice(0, 50),
        searchPending: false,
        catalogueEnd: false,
      };
    case END_OF_CATALOGUE:
      return {
        ...state,
        visibleUsers: state.allUsers,
        catalogueEnd: true,
      };
    case UPDATE_NATIONALITY: {
      return {
        ...state,
        nationality: action.payload,
      };
    }
    case SCHEDULE_SEARCH:
      return {
        ...state,
        searchPending: true,
      };
    case UPDATE_QUERY: {
      return {
        ...state,
        searchQuery: action.payload,
      };
    }
    case SHOW_NEXT_USERS: {
      return {
        ...state,
        visibleUsers: state.allUsers,
      };
    }
    case ADD_NEXT_USERS:
      return {
        ...state,
        currentBatch: state.currentBatch + 1,
        allUsers: state.allUsers.concat(action.payload),
        searchPending: false,
      };
    case RESET_STATE: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
};

export default applicationReducer;
