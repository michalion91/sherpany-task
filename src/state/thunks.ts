import { ThunkAction } from "redux-thunk";

import { fetchUserBatch } from "~/network";
import {
  addNextUsers,
  endOfCatalogue,
  loadInitialData,
  scheduleSearch,
  showNextUsers,
  updateNationality,
} from "./actions";

import {
  AddNextUsersAction,
  EndOfCatalogueAction,
  LoadInitialDataAction,
  ScheduleSearchAction,
  ShowNextUsersAction,
  SupportedNationality,
  UpdateNationalityAction,
} from "./types";

import { selectBatchDetails, selectNationality, isLoading } from "./selectors";
import { ApplicationState } from "./reducer";

export const appStartup = (): ThunkAction<
  void,
  ApplicationState,
  unknown,
  LoadInitialDataAction
> => (dispatch, getState) => {
  const appState = getState();
  const nationality = selectNationality(appState);
  const { current } = selectBatchDetails(appState);

  const fetchInitial = () =>
    fetchUserBatch(nationality, current, 100).then((data) => {
      if (data.success) {
        dispatch(loadInitialData(data.payload));
      } else {
        setTimeout(() => {
          fetchInitial();
        }, 2000);
      }
    });

  fetchInitial();
};

export const loadNextUsers = (): ThunkAction<
  void,
  ApplicationState,
  unknown,
  | AddNextUsersAction
  | EndOfCatalogueAction
  | ScheduleSearchAction
  | ShowNextUsersAction
> => (dispatch, getState) => {
  const appState = getState();

  const nationality = selectNationality(appState);
  const { current, max } = selectBatchDetails(appState);
  const loading = isLoading(appState);

  if (current + 1 >= max) {
    dispatch(endOfCatalogue());
    return;
  }

  if (loading) {
    setTimeout(() => loadNextUsers()(dispatch, getState, undefined), 2000);
    return;
  }

  dispatch(showNextUsers());

  dispatch(scheduleSearch());

  const fetchNext = () =>
    fetchUserBatch(nationality, current + 1).then((data) => {
      if (data.success) {
        dispatch(addNextUsers(data.payload));
      } else {
        setTimeout(() => {
          fetchNext();
        }, 2000);
      }
    });

  fetchNext();
};

export const changeNationality = (
  newNationality: SupportedNationality
): ThunkAction<
  void,
  ApplicationState,
  unknown,
  LoadInitialDataAction | ScheduleSearchAction | UpdateNationalityAction
> => (dispatch) => {
  dispatch(scheduleSearch());

  const fetchInitial = () =>
    fetchUserBatch(newNationality, 1, 100).then((data) => {
      if (data.success) {
        dispatch(loadInitialData(data.payload));
      } else {
        setTimeout(() => {
          fetchInitial();
        }, 2000);
      }
    });

  dispatch(updateNationality(newNationality));
  fetchInitial();
};
