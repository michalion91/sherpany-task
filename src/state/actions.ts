import {
  ADD_NEXT_USERS,
  AddNextUsersAction,
  END_OF_CATALOGUE,
  EndOfCatalogueAction,
  LOAD_INITIAL_DATA,
  LoadInitialDataAction,
  SCHEDULE_SEARCH,
  ScheduleSearchAction,
  SHOW_NEXT_USERS,
  ShowNextUsersAction,
  SupportedNationality,
  UPDATE_NATIONALITY,
  UPDATE_QUERY,
  UpdateNationalityAction,
  UpdateQueryAction,
  RESET_STATE,
  ResetState,
} from "./types";
import { User } from "~/network";

export const loadInitialData = (users: User[]): LoadInitialDataAction => ({
  type: LOAD_INITIAL_DATA,
  payload: users,
});

export const scheduleSearch = (): ScheduleSearchAction => ({
  type: SCHEDULE_SEARCH,
});

export const addNextUsers = (nextUsers: User[]): AddNextUsersAction => ({
  type: ADD_NEXT_USERS,
  payload: nextUsers,
});

export const showNextUsers = (): ShowNextUsersAction => ({
  type: SHOW_NEXT_USERS,
});

export const endOfCatalogue = (): EndOfCatalogueAction => ({
  type: END_OF_CATALOGUE,
});

export const updateNationality = (
  nationality: SupportedNationality
): UpdateNationalityAction => ({
  type: UPDATE_NATIONALITY,
  payload: nationality,
});

export const updateQuery = (newQuery: string): UpdateQueryAction => ({
  type: UPDATE_QUERY,
  payload: newQuery,
});

export const resetState = (): ResetState => ({
  type: RESET_STATE,
});
