import { User } from "network";

const testUser = (uuid = "test-uuid"): User => ({
  cell: 123,
  email: "test@email.com",
  location: {
    city: "ZH",
    postcode: 8000,
    state: "CH",
    street: {
      name: "test street",
      number: 7,
    },
  },
  login: {
    username: "test-username",
    uuid: uuid,
  },
  name: {
    first: "test-first-name",
    last: "test-last-name",
  },
  phone: 321,
  picture: {
    thumbnail: "test-thumbnail-url-string",
  },
});

export default testUser;
