import testUser from "./__test__/mockUserGenerator";

import reducer, { INITIAL_STATE, ApplicationState } from "./reducer";
import {
  SupportedNationality,
  UPDATE_NATIONALITY,
  SCHEDULE_SEARCH,
  ADD_NEXT_USERS,
  END_OF_CATALOGUE,
  LOAD_INITIAL_DATA,
  UPDATE_QUERY,
  SHOW_NEXT_USERS,
} from "./types";
import {
  updateNationality,
  scheduleSearch,
  addNextUsers,
  endOfCatalogue,
  loadInitialData,
  updateQuery,
  showNextUsers,
} from "./actions";

describe("applicationReducer", () => {
  test("returns initial state if is not provided and action is unsupported", () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const result = reducer(undefined, {} as any);

    expect(result).toEqual(INITIAL_STATE);
  });

  test("returns unchanged state in case of unsupported action", () => {
    const exampleState: ApplicationState = {
      ...INITIAL_STATE,
      nationality: SupportedNationality.FR,
    };

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const result = reducer(exampleState, {} as any);

    expect(exampleState).toEqual(result);
  });

  test(`action: ${UPDATE_NATIONALITY}`, () => {
    const result = reducer(
      undefined,
      updateNationality(SupportedNationality.ES)
    );

    expect(result).toEqual({
      ...INITIAL_STATE,
      nationality: SupportedNationality.ES,
    });

    const initialState = {
      ...INITIAL_STATE,
      nationality: SupportedNationality.CH,
    };

    const referenceCheckResult = reducer(
      initialState,
      updateNationality(SupportedNationality.CH)
    );

    expectResultAndPurity(
      referenceCheckResult,
      {
        ...INITIAL_STATE,
        nationality: SupportedNationality.CH,
      },
      initialState
    );
  });

  test(`action: ${LOAD_INITIAL_DATA}`, () => {
    const userList = new Array(100)
      .fill(0)
      .map((_value, index) => testUser(`${index}`));

    const result = reducer(undefined, loadInitialData(userList));

    expect(result).toEqual({
      ...INITIAL_STATE,
      currentBatch: 2,
      allUsers: userList,
      visibleUsers: userList.slice(0, 50),
      searchPending: false,
    });

    const referenceCheckResult = reducer(
      INITIAL_STATE,
      loadInitialData(userList)
    );

    expectResultAndPurity(
      referenceCheckResult,
      {
        ...INITIAL_STATE,
        currentBatch: 2,
        allUsers: userList,
        visibleUsers: userList.slice(0, 50),
        searchPending: false,
      },
      INITIAL_STATE
    );
  });

  test(`action: ${SCHEDULE_SEARCH}`, () => {
    const result = reducer(undefined, scheduleSearch());

    expect(result).toEqual({ ...INITIAL_STATE, searchPending: true });

    const referenceCheckResult = reducer(INITIAL_STATE, scheduleSearch());

    expectResultAndPurity(
      referenceCheckResult,
      {
        ...INITIAL_STATE,
        searchPending: true,
      },
      INITIAL_STATE
    );
  });

  test(`action: ${ADD_NEXT_USERS}`, () => {
    const result = reducer(undefined, addNextUsers([testUser()]));

    expect(result).toEqual({
      ...INITIAL_STATE,
      currentBatch: 2,
      visibleUsers: [],
      allUsers: [testUser()],
      searchPending: false,
    });

    const initialState = {
      ...INITIAL_STATE,
      allUsers: [testUser("1")],
    };

    const referenceCheckResult = reducer(
      initialState,
      addNextUsers([testUser("2")])
    );

    expectResultAndPurity(
      referenceCheckResult,
      {
        ...INITIAL_STATE,
        currentBatch: 2,
        allUsers: [testUser("1"), testUser("2")],
        searchPending: false,
      },
      initialState
    );
  });

  test(`action ${SHOW_NEXT_USERS}`, () => {
    const initialState = {
      ...INITIAL_STATE,
      allUsers: [testUser("1")],
    };

    const referenceCheckResult = reducer(initialState, showNextUsers());

    expectResultAndPurity(
      referenceCheckResult,
      {
        ...INITIAL_STATE,
        visibleUsers: initialState.allUsers,
        allUsers: initialState.allUsers,
      },
      initialState
    );
  });

  test(`action ${END_OF_CATALOGUE}`, () => {
    const result = reducer(undefined, endOfCatalogue());

    expect(result).toEqual({
      ...INITIAL_STATE,
      catalogueEnd: true,
    });

    const initialState = {
      ...INITIAL_STATE,
      allUsers: [testUser("1")],
    };

    const referenceCheckResult = reducer(initialState, endOfCatalogue());

    expectResultAndPurity(
      referenceCheckResult,
      {
        ...INITIAL_STATE,
        visibleUsers: [testUser("1")],
        allUsers: [testUser("1")],
        catalogueEnd: true,
      },
      initialState
    );
  });

  test(`action ${UPDATE_QUERY}`, () => {
    const testText = "test";
    const referenceCheckResult = reducer(INITIAL_STATE, updateQuery(testText));

    expectResultAndPurity(
      referenceCheckResult,
      {
        ...INITIAL_STATE,
        searchQuery: testText,
      },
      INITIAL_STATE
    );
  });
});

function expectResultAndPurity(
  result: ApplicationState,
  expected: ApplicationState,
  referenceCheck: ApplicationState
) {
  expect(result).toEqual(expected);
  expect(result).not.toBe(referenceCheck);
}
