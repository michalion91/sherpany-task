import { Action } from "redux";

import { User } from "~/network";

export enum SupportedNationality {
  CH = "CH",
  ES = "ES",
  FR = "FR",
  GB = "GB",
}

export const LOAD_INITIAL_DATA = "LOAD_INITIAL_DATA";
export const SCHEDULE_SEARCH = "SCHEDULE_SEARCH";
export const ADD_NEXT_USERS = "ADD_NEXT_USERS";
export const SHOW_NEXT_USERS = "SHOW_NEXT_USERS";
export const END_OF_CATALOGUE = "END_OF_CATALOGUE";
export const UPDATE_NATIONALITY = "UPDATE_NATIONALITY";
export const UPDATE_QUERY = "UPDATE_QUERY";
export const RESET_STATE = "RESET_STATE";

export interface LoadInitialDataAction
  extends Action<typeof LOAD_INITIAL_DATA> {
  payload: User[];
}

export interface UpdateNationalityAction
  extends Action<typeof UPDATE_NATIONALITY> {
  payload: SupportedNationality;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ScheduleSearchAction extends Action<typeof SCHEDULE_SEARCH> {}

export interface AddNextUsersAction extends Action<typeof ADD_NEXT_USERS> {
  payload: User[];
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ShowNextUsersAction extends Action<typeof SHOW_NEXT_USERS> {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface EndOfCatalogueAction extends Action<typeof END_OF_CATALOGUE> {}

export interface UpdateNationalityAction
  extends Action<typeof UPDATE_NATIONALITY> {
  payload: SupportedNationality;
}

export interface UpdateQueryAction extends Action<typeof UPDATE_QUERY> {
  payload: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ResetState extends Action<typeof RESET_STATE> {}

export type ApplicationActionTypes =
  | AddNextUsersAction
  | EndOfCatalogueAction
  | LoadInitialDataAction
  | ResetState
  | ScheduleSearchAction
  | ShowNextUsersAction
  | UpdateNationalityAction
  | UpdateQueryAction;
