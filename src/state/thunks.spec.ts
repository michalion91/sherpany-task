import { appStartup, loadNextUsers, changeNationality } from "./thunks";
import { INITIAL_STATE } from "./reducer";
import {
  loadInitialData,
  showNextUsers,
  scheduleSearch,
  addNextUsers,
  endOfCatalogue,
  updateNationality,
} from "./actions";
import testUser from "./__test__/mockUserGenerator";
import { SupportedNationality } from "./types";

global.fetch = jest.fn();

const users = [testUser("1")];
// NOTE: discussion point on testing thunks
jest.setTimeout(30000);

describe("thunk appStartup", () => {
  test("schedules network call and updates store with new data", (done) => {
    (fetch as jest.Mock<unknown>).mockImplementationOnce((url: string) => {
      expect(url).toEqual(generateUrl(1, 100));

      return successfulFetch();
    });

    const mockedDispatch = jest.fn();

    mockedDispatch.mockImplementationOnce((arg) => {
      expect(arg).toEqual(loadInitialData(users));
      done();
    });

    appStartup()(mockedDispatch, () => INITIAL_STATE, undefined);
  });

  test("schedules network call and retries on failure", (done) => {
    (fetch as jest.Mock<unknown>)
      .mockImplementationOnce((url: string) => {
        expect(url).toEqual(generateUrl(1, 100));

        return failedFetch();
      })
      .mockImplementationOnce((url: string) => {
        expect(url).toEqual(generateUrl(1, 100));

        return successfulFetch();
      });

    const mockedDispatch = jest.fn();

    mockedDispatch.mockImplementationOnce((arg) => {
      expect(arg).toEqual(loadInitialData(users));
      done();
    });

    appStartup()(mockedDispatch, () => INITIAL_STATE, undefined);
  });
});

describe("thunk loadNextUsers", () => {
  test("shows prefetched batch of users, and prefetches next one", (done) => {
    (fetch as jest.Mock<unknown>).mockImplementationOnce((url: string) => {
      expect(url).toEqual(generateUrl(2, 50));

      return successfulFetch();
    });

    const mockedDispatch = jest.fn();

    mockedDispatch
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(showNextUsers());
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(scheduleSearch());
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(addNextUsers(users));
        done();
      });

    loadNextUsers()(
      mockedDispatch,
      () => ({ ...INITIAL_STATE, searchPending: false }),
      undefined
    );
  });

  test("shows prefetched batch of users, and prefetches next one including retry mechanism", (done) => {
    (fetch as jest.Mock<unknown>)
      .mockImplementationOnce((url: string) => {
        expect(url).toEqual(generateUrl(2, 50));

        return failedFetch();
      })
      .mockImplementationOnce((url: string) => {
        expect(url).toEqual(generateUrl(2, 50));

        return successfulFetch();
      });

    const mockedDispatch = jest.fn();

    mockedDispatch
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(showNextUsers());
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(scheduleSearch());
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(addNextUsers(users));
        done();
      });

    loadNextUsers()(
      mockedDispatch,
      () => ({ ...INITIAL_STATE, searchPending: false }),
      undefined
    );
  });

  test("signals end of catalogue in case of 1000 entries", (done) => {
    const mockedDispatch = jest.fn();

    mockedDispatch.mockImplementationOnce((arg) => {
      expect(arg).toEqual(endOfCatalogue());
      done();
    });

    loadNextUsers()(
      mockedDispatch,
      () => ({ ...INITIAL_STATE, currentBatch: 21 }),
      undefined
    );
  });

  test("in case of multiple thunks in parallel, it awaits the lock flag to be released", (done) => {
    (fetch as jest.Mock<unknown>).mockImplementationOnce((url: string) => {
      expect(url).toEqual(generateUrl(2, 50));

      return successfulFetch();
    });

    const mockedState = jest.fn();
    mockedState
      .mockReturnValueOnce({
        ...INITIAL_STATE,
        searchPending: true,
      })
      .mockReturnValueOnce({
        ...INITIAL_STATE,
        searchPending: false,
      });

    const mockedDispatch = jest.fn();
    mockedDispatch
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(showNextUsers());
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(scheduleSearch());
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(addNextUsers(users));
        done();
      });

    loadNextUsers()(mockedDispatch, mockedState, undefined);
  });
});

describe("thunk changeNationality", () => {
  test("schedules search with new nationality", (done) => {
    const nationality = SupportedNationality.GB;

    (fetch as jest.Mock<unknown>).mockImplementationOnce((url: string) => {
      expect(url).toEqual(generateUrl(1, 100, nationality));
      return successfulFetch();
    });

    const mockedDispatch = jest.fn();

    mockedDispatch
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(scheduleSearch());
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(updateNationality(nationality));
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(loadInitialData(users));
        done();
      });

    changeNationality(nationality)(
      mockedDispatch,
      () => INITIAL_STATE,
      undefined
    );
  });

  test("schedules search with new nationality with retry mechanism", (done) => {
    const nationality = SupportedNationality.GB;

    (fetch as jest.Mock<unknown>)
      .mockImplementationOnce((url: string) => {
        expect(url).toEqual(generateUrl(1, 100, nationality));

        return failedFetch();
      })
      .mockImplementationOnce((url: string) => {
        expect(url).toEqual(generateUrl(1, 100, nationality));

        return successfulFetch();
      });

    const mockedDispatch = jest.fn();

    mockedDispatch
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(scheduleSearch());
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(updateNationality(nationality));
      })
      .mockImplementationOnce((arg) => {
        expect(arg).toEqual(loadInitialData(users));
        done();
      });

    changeNationality(nationality)(
      mockedDispatch,
      () => INITIAL_STATE,
      undefined
    );
  });
});

function generateUrl(
  batch: number,
  results: number,
  nationality = SupportedNationality.CH
) {
  return `https://randomuser.me/api/?results=${results}&nationality=${nationality}&page=${batch}&inc=login%2Cname%2Clocation%2Cphone%2Ccell%2Cpicture%2Cemail`;
}

function successfulFetch(): unknown {
  return Promise.resolve({
    json: () => Promise.resolve({ results: users }),
  });
}

function failedFetch(): unknown {
  return Promise.resolve({
    json: () => Promise.reject({}),
  });
}
