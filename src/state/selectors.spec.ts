import testUser from "./__test__/mockUserGenerator";

import { INITIAL_STATE } from "./reducer";
import {
  selectNationality,
  selectVisibleUsers,
  selectBatchDetails,
  isEndOfCatalogue,
  selectSearchQuery,
  isLoading,
} from "./selectors";
import { SupportedNationality } from "./types";
import { User } from "~/network";

describe("selectors", () => {
  test("nationality", () => {
    expect(selectNationality(INITIAL_STATE)).toEqual(SupportedNationality.CH);
  });

  test("visible users", () => {
    const someUsers: User[] = [testUser("1")];
    expect(selectVisibleUsers(INITIAL_STATE)).toEqual([]);
    expect(
      selectVisibleUsers({ ...INITIAL_STATE, visibleUsers: someUsers })
    ).toEqual(someUsers);
  });

  test("batch details", () => {
    expect(selectBatchDetails(INITIAL_STATE)).toEqual({
      current: 1,
      max: 22,
    });
  });

  test("end of catalogue", () => {
    expect(isEndOfCatalogue(INITIAL_STATE)).toEqual(false);
    expect(isEndOfCatalogue({ ...INITIAL_STATE, catalogueEnd: true })).toEqual(
      true
    );
  });

  test("search query", () => {
    const testText = "test";
    expect(selectSearchQuery(INITIAL_STATE)).toEqual("");
    expect(
      selectSearchQuery({ ...INITIAL_STATE, searchQuery: testText })
    ).toEqual(testText);
  });

  test("is loading", () => {
    expect(isLoading(INITIAL_STATE)).toEqual(true);
    expect(isLoading({ ...INITIAL_STATE, searchPending: false })).toEqual(
      false
    );
  });
});
